<?php

    class Student {

        private string $name;
        private string $lastName;
        private int $age;
        private string $course;

        public function __construct($name, $lastName, $age, $course) {
            $this->name = $name;
            $this->lastName = $lastName;
            $this->age = $age;
            $this->course = $course;
        }


        public function getName() {
            return $this->name;
        }

        public function getLastName() {
            return $this->lastName;
        }

        public function getAge() {
            return $this->age;
        }

        public function getCourse() {
            return $this->course;
        }

        public function getInfo() {
            return 'Студент ' . $this->name . ' ' . $this->lastName . ', ' . $this->age . ' лет, ' . $this->course;;
        }


        public function setName($name) {
            return $this->name = $name;
        }

        public function setLastName($lastName) {
            return $this->lastName = $lastName;
        }

        public function setAge($age) {
            return $this->age = $age;
        }

        public function setCourse($course) {
            return $this->course = $course;
        }
    }

    $student = new Student('Петр', 'Иванов', 35, 'инженер');
    echo $student->getName() . '<br>';
    echo $student->getLastName() . '<br>';
    echo $student->getAge() . '<br>';
    echo $student->getCourse() . '<br>';
    echo $student->getInfo() . '<br><br>';

    $student->setName('Евгений');
    $student->setLastName('Петров');
    $student->setAge(20);
    $student->setCourse('Конструктор');
    echo $student->getInfo() . '<br><br>';



    class Car {

        private string $brand;
        private string $model;
        private string $color;
        private int $year;

        public function __construct($brand, $model, $color, $year) {
            $this->brand = $brand;
            $this->model = $model;
            $this->color = $color;
            $this->year = $year;
        }


        public function getBrand() {
            return $this->brand;
        }

        public function getModel() {
            return $this->model;
        }

        public function getColor() {
            return $this->color;
        }

        public function getYear() {
            return $this->year;
        }

        public function getInfo() {
            return 'Машина бренда ' . $this->brand . ', модель ' . $this->model . ', цвет ' . $this->color . ', год ' . $this->year;;
        }


        public function setBrand($brand) {
            return $this->brand = $brand;
        }

        public function setModel($model) {
            return $this->model = $model;
        }

        public function setColor($color) {
            return $this->color = $color;
        }

        public function setYear($year) {
            return $this->year = $year;
        }
    }

    $car = new Car('Mercedes', 'AMG', 'silver', 2004);
    echo $car->getBrand() . '<br>';
    echo $car->getModel() . '<br>';
    echo $car->getColor() . '<br>';
    echo $car->getYear() . '<br>';
    echo $car->getInfo() . '<br><br>';

    $car->setBrand('Opel');
    $car->setModel('Insignia');
    $car->setColor('black');
    $car->setYear(2012);
    echo $car->getInfo() . '<br><br>';




    class Circle {

        private int $radius;

        public function __construct($radius){
            $this->radius = $radius;
        }

        public function calculateArea() {
            return pi() * $this->radius ** 2;
        }
    }

    $circle = new Circle(3);
    echo $circle->calculateArea() . '<br><br>';



    class Human {

        private string $name;
        private int $age;
        private string $gender;

        public function __construct($name, $age, $gender) {
            $this->name = $name;
            $this->age = $age;
            $this->gender = $gender;
        }

        public function isAdult() {
            if ($this->age >= 18) {
                return 'true';
            } 
            else {
                return 'false';
            }
        }
    }

    $human = new Human('Genevieve', 18, 'f');
    echo $human->isAdult();
